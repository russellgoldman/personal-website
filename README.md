# Personal-Website
https://russellgoldman.me/

This is the GitHub repository for my Personal Website. It is built using ReactJS, Flexbox, and GitHub Pages for hosting.

## Contributions:
**Russell Goldman:** Front-End Development

**Grace Ma:** Design Mockups
