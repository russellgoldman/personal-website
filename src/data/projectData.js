import {
  gelatoBanner,
  novaBanner,
  rgBanner,
  gelato,
  nova,
  rg
} from './../images/projects';

export const projectData = [
  {
    id: 0,
    name: 'Gelato',
    title: 'Gelato - Gamified Education Tool',
    description: 'Gelato is a ReactJS web app that allows teachers to put together education content such as quizzes and other practice material into a dashboard which the app then uses to populate questions with a game. The student section functions like an RPG game based on the content the teacher has made for their class. It was built at the Equithon Hackathon hosted at the University of Waterloo in a team of four.',
    github: 'https://github.com/russellgoldman/Project-Gelato',
    bannerImgPath: `${gelatoBanner}`,
    profileImgPath: `${gelato}`,
    toolsUsed: [
      'JavaScript',
      'ReactJS',
      'HTML',
      'CSS'
    ]
  },
  {
    id: 1,
    name: 'Nova',
    title: 'Nova - AI Mental Health Chatbot',
    description: 'Gelato is a ReactJS web app that allows teachers to put together education content such as quizzes and other practice material into a dashboard which the app then uses to populate questions with a game. The student section functions like an RPG game based on the content the teacher has made for their class. It was built at the Equithon Hackathon hosted at the University of Waterloo in a team of four.',
    github: 'https://github.com/russellgoldman/Nova',
    bannerImgPath: `${novaBanner}`,
    profileImgPath: `${nova}`,
    toolsUsed: ['Java', 'Android', 'NodeJS']
  },
  {
    id: 2,
    name: 'Portfolio Site',
    title: 'Portfolio Site',
    description: 'Gelato is a ReactJS web app that allows teachers to put together education content such as quizzes and other practice material into a dashboard which the app then uses to populate questions with a game. The student section functions like an RPG game based on the content the teacher has made for their class. It was built at the Equithon Hackathon hosted at the University of Waterloo in a team of four.',
    github: 'https://github.com/russellgoldman/Personal-Website',
    bannerImgPath: `${rgBanner}`,
    profileImgPath: `${rg}`,
    toolsUsed: [
      'JavaScript',
      'ReactJS',
      'HTML',
      'CSS'
    ]
  },
  {
    id: 3,
    name: 'React Native App',
    title: 'Scribble - React Native App',
    description: 'Gelato is a ReactJS web app that allows teachers to put together education content such as quizzes and other practice material into a dashboard which the app then uses to populate questions with a game. The student section functions like an RPG game based on the content the teacher has made for their class. It was built at the Equithon Hackathon hosted at the University of Waterloo in a team of four.',
    github: 'https://github.com/russellgoldman/Project-Gelato',
    bannerImgPath: `${novaBanner}`,
    profileImgPath: `${gelato}`,
    toolsUsed: [
      'JavaScript',
      'React Native',
      'Redux',
      'NodeJS',
      'MongoDB',
      'Mocha',
    ]
  },
  {
    id: 4,
    name: 'Project 4',
    title: 'Gelato - Gamified Education Tool',
    description: '',
    github: 'https://github.com/russellgoldman/Project-Gelato',
    bannerImgPath: `${novaBanner}`,
    profileImgPath: `${gelato}`,
    toolsUsed: [
      'JavaScript',
      'ReactJS',
      'HTML',
      'CSS'
    ]
  },
  {
    id: 5,
    name: 'Project 5',
    title: 'Nova - AI Mental Health Chatbot',
    description: '',
    github: 'https://github.com/russellgoldman/Project-Gelato',
    bannerImgPath: `${novaBanner}`,
    profileImgPath: `${gelato}`,
    toolsUsed: [
      'JavaScript',
      'ReactJS',
      'HTML',
      'CSS'
    ]
  },
  {
    id: 6,
    name: 'Project 6',
    title: 'Portfolio Site',
    description: '',
    github: 'https://github.com/russellgoldman/Project-Gelato',
    bannerImgPath: `${novaBanner}`,
    profileImgPath: `${gelato}`,
    toolsUsed: [
      'JavaScript',
      'ReactJS',
      'HTML',
      'CSS'
    ]
  },
  {
    id: 7,
    name: 'Project 7',
    title: 'Scribble - React Native Application',
    description: '',
    github: 'https://github.com/russellgoldman/Project-Gelato',
    bannerImgPath: `${novaBanner}`,
    profileImgPath: `${gelato}`,
    toolsUsed: [
      'JavaScript',
      'ReactJS',
      'HTML',
      'CSS'
    ]
  },
  {
    id: 8,
    name: 'Project 8',
    title: 'Gelato - Gamified Education Tool',
    description: '',
    github: 'https://github.com/russellgoldman/Project-Gelato',
    bannerImgPath: `${novaBanner}`,
    profileImgPath: `${gelato}`,
    toolsUsed: [
      'JavaScript',
      'ReactJS',
      'HTML',
      'CSS'
    ]
  },
  {
    id: 9,
    name: 'Project 9',
    title: 'Nova - AI Mental Health Chatbot',
    description: '',
    github: 'https://github.com/russellgoldman/Project-Gelato',
    bannerImgPath: `${novaBanner}`,
    profileImgPath: `${gelato}`,
    toolsUsed: [
      'JavaScript',
      'ReactJS',
      'HTML',
      'CSS'
    ]
  },
  {
    id: 10,
    name: 'Project 10',
    title: 'Portfolio Site',
    description: '',
    github: 'https://github.com/russellgoldman/Project-Gelato',
    bannerImgPath: `${novaBanner}`,
    profileImgPath: `${gelato}`,
    toolsUsed: [
      'JavaScript',
      'ReactJS',
      'HTML',
      'CSS'
    ]
  },
  {
    id: 11,
    name: 'Project 11',
    title: 'Scribble - React Native Application',
    description: '',
    github: 'https://github.com/russellgoldman/Project-Gelato',
    bannerImgPath: `${novaBanner}`,
    profileImgPath: `${gelato}`,
    toolsUsed: [
      'JavaScript',
      'ReactJS',
      'HTML',
      'CSS'
    ]
  }
];
