import {
  android,
  c,
  express,
  Java,
  js,
  mocha,
  mongodb,
  node,
  python,
  react,
  reactnative,
  sql,
  django,
  redux,
  html,
  css,
  sass,
  ios,
  swift
} from './../images/toolkit-icons';

export const ToolkitIcons = [
  {
    id: '0',
    name: 'JavaScript',
    imgPath: `${js}`
  },
  {
    id: '1',
    name: 'ReactJS',
    imgPath: `${react}`
  },
  {
    id: '2',
    name: 'React Native',
    imgPath: `${reactnative}`
  },
  {
    id: '3',
    name: 'NodeJS',
    imgPath: `${node}`
  },
  {
    id: '4',
    name: 'Python',
    imgPath: `${python}`
  },
  {
    id: '5',
    name: 'Java',
    imgPath: `${Java}`
  },
  {
    id: '6',
    name: 'C',
    imgPath: `${c}`
  },
  {
    id: '7',
    name: 'Express',
    imgPath: `${express}`
  },
  {
    id: '8',
    name: 'MongoDB',
    imgPath: `${mongodb}`
  },
  {
    id: '9',
    name: 'Redux',
    imgPath: `${redux}`
  },
  {
    id: '10',
    name: 'Android',
    imgPath: `${android}`
  },
  {
    id: '11',
    name: 'Django',
    imgPath: `${django}`
  },
  {
    id: '12',
    name: 'SQL',
    imgPath: `${sql}`
  },
  {
    id: '13',
    name: 'Mocha',
    imgPath: `${mocha}`
  },
  {
    id: '14',
    name: 'HTML',
    imgPath: `${html}`
  },
  {
    id: '15',
    name: 'CSS',
    imgPath: `${css}`
  },
  {
    id: '16',
    name: 'Sass',
    imgPath: `${sass}`
  },
  // {
  //   id: '17',
  //   name: 'iOS',
  //   imgPath: `${ios}`
  // },
  // {
  //   id: '18',
  //   name: 'Swift',
  //   imgPath: `${swift}`
  // },
];
