import websiteLogo from './website-logo.svg';
import rightArrow from './right-arrow.svg';
import leftArrow from './left-arrow.svg';
import bioImage from './bioImage.jpg';
import websiteLogoWhite from './website-logo-white.png';

export {
  websiteLogo,
  rightArrow,
  leftArrow,
  bioImage,
  websiteLogoWhite,
};
