import gelatoBanner from './gelatoBanner.png';
import novaBanner from './novaBanner.png';
import rgBanner from './rgBanner.png';
import gelato from './gelato.svg';
import nova from './nova.png';
import rg from './rg.png';

export {
  gelatoBanner,
  novaBanner,
  rgBanner,
  gelato,
  nova,
  rg
}
