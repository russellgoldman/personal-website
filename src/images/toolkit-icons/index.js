import android from './android.svg';
import c from './c.svg';
import express from './express.svg';
import Java from './java.svg';
import js from './js.svg';
import mocha from './mocha.svg';
import mongodb from './mongodb.svg';
import node from './node.svg';
import python from './python.svg';
import react from './react.svg';
import sql from './sql.svg';
import django from './django.svg';
import redux from './redux.svg';
import html from './html.svg';
import css from './css.svg';
import sass from './sass.svg';
import reactnative from './reactnative.png';
import ios from './ios.svg';
import swift from './swift.svg';

export {
  android,
  c,
  express,
  Java,
  js,
  mocha,
  mongodb,
  node,
  python,
  react,
  sql,
  django,
  redux,
  html,
  css,
  sass,
  reactnative,
  ios,
  swift
};
