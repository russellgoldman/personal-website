import linkedin from './linkedin.svg';
import github from './github.svg';
import medium from './medium.svg';
import linkedinHover from './linkedin-hover.svg';
import githubHover from './github-hover.svg';
import mediumHover from './medium-hover.svg';

export {
  linkedin,
  github,
  medium,
  linkedinHover,
  githubHover,
  mediumHover
}
