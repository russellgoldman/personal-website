export const styles = {
  outerContainer: {
    marginTop: '7.5vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  imageContainer: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'center',
    marginBottom: '1vh'
  },
  bioImage: {
    width: '300px',
    height: '300px',
    borderRadius: '25px'
  },
  textContainer: {
    marginTop: '2.5vh',
    display: 'flex',
    flex: 1,
    fontFamily: 'Roboto-Light',
    fontSize: '16px',
    lineHeight: '2.5',
    justifyContent: 'center',
    textAlign: 'center',
    marginLeft: '10vw',
    marginRight: '10vw',
    marginBottom: '4vh'
  },
  buttonContainer: {
    display: 'flex',
    flex: 2,
    marginLeft: '25vw',
    marginRight: '25vw'
  },
  linkContainer: {
    display: 'flex',
    flex: 1,
    textDecoration: 'none'
  },
  button: {
    fontSize: '26px',
    fontFamily: 'Roboto',
    letterSpacing: '3px',
    flex: 3,
    backgroundColor: '#00ABCD',
    color: 'white',
    borderRadius: '10px',
    justifyContent: 'center',
    height: '60px'
  }
};
