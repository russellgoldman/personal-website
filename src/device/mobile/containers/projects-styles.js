export const styles = {
  projectContainer: {
    marginTop: '-0.8em',
    height: '90vh'
  },
  projectCarouselContainer: {
    paddingTop: '0.5em',
  }
};
