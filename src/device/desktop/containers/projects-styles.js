export const styles = {
  projectContainer: {
    marginTop: '-0.8em',
    height: '80vh',
    marginBottom: '7.5em',
  },
  projectCarouselContainer: {
    paddingTop: '0.5em',
  }
};
