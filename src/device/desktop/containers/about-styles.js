export const styles = {
  outerContainer: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    marginLeft: '10em',
    marginRight: '10vw'
  },
  innerContainer: {
    marginTop: '5vw',
    display: 'flex',
    flex: 4,
    flexDirection: 'row',
  },
  carouselContainer: {
    flex: 1
  },
  imageContainer: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    maxWidth: '25em'
  },
  bioContainer: {
    display: 'flex',
    flex: 3,
    flexDirection: 'column',
  },
  textContainer: {
    display: 'flex',
    flexDirection: 'column',
    lineHeight: '2',
    marginTop: '-0.1vw'
  },
  textSmall: {
    fontFamily: 'Roboto',
    flex: 1,
    fontSize: '14.5px',
    marginLeft: '3.5vw'
  },
  textMedium: {
    fontFamily: 'Roboto-Light',
    flex: 1,
    fontSize: '18px',
    marginLeft: '5vw'
  },
  textLarge: {
    fontFamily: 'Roboto-Light',
    flex: 1,
    fontSize: '22.5px',
    marginLeft: '8vw'
  },
  bioImageSmall: {
    width: '300px',
    height: '300px',
    borderRadius: '30px',
    marginLeft: '-3.5vw'
  },
  bioImageMedium: {
    width: '350px',
    height: '350px',
    borderRadius: '30px',
    marginLeft: '2vw',
  },
  bioImageLarge: {
    width: '400px',
    height: '400px',
    borderRadius: '30px',
    marginLeft: '5vw',
  },
  buttonSmall: {
    fontSize: '22px',
    fontFamily: 'Roboto',
    flex: 1,
    letterSpacing: '3px',
    backgroundColor: '#00ABCD',
    color: 'white',
    borderRadius: '10px',
    height: '40px',
    maxWidth: '25%',
    outline: 'none',
    marginTop: '1em'
  },
  buttonMedium: {
    fontSize: '22px',
    fontFamily: 'Roboto',
    flex: 1,
    letterSpacing: '3px',
    backgroundColor: '#00ABCD',
    color: 'white',
    borderRadius: '10px',
    height: '45px',
    maxWidth: '25%',
    outline: 'none',
    marginTop: '1.25em'
  },
  buttonLarge: {
    fontSize: '30px',
    fontFamily: 'Roboto',
    flex: 1,
    letterSpacing: '3px',
    backgroundColor: '#00ABCD',
    color: 'white',
    borderRadius: '10px',
    height: '60px',
    maxWidth: '30%',
    outline: 'none',
    marginTop: '1.25em'
  },
  linkContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    textDecoration: 'none',
    justifyContent: 'center',
    alignItems: 'center',
  }
};
